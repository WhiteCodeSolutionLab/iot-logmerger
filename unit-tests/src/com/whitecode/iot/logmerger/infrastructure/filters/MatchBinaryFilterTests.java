/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure.filters;

import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.entities.filters.LogEntryFieldType;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDateTime;

public class MatchBinaryFilterTests {

    private final static LocalDateTime DATE_TIME = LocalDateTime.of(2015, 12, 6, 0, 0);

    private final static String DEVICE_NAME = "Device";
    private final static String DEVICE_NAME_VALID = "Device 121";

    private final static String MESSAGE_TYPE = "Type";

    private final static String MESSAGE = "Message";
    private final static String MESSAGE_VALID = "Message 121";

    private String regular = "\\b%s\\b.*[\\d]+";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void ctor_nullFieldTypeArgument_shouldThrownException() {
        thrown.expect(IllegalArgumentException.class);

        MatchBinaryFilter filter = new MatchBinaryFilter(null, DEVICE_NAME);
    }

    @Test
    public void ctor_nullFieldValueArgument_shouldThrownException() {
        thrown.expect(IllegalArgumentException.class);

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.device, null);
    }

    @Test
    public void ctor_unsupportedFieldTimestamp_shouldThrownException() {
        thrown.expect(UnsupportedOperationException.class);

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.timestamp, DATE_TIME);
    }

    @Test
    public void ctor_unsupportedFieldMessageType_shouldThrownException() {
        thrown.expect(UnsupportedOperationException.class);

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.type, MESSAGE_TYPE);
    }

    @Test
    public void ctor_FieldDeviceName_shouldPass() {

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.device, regular);
    }

    @Test
    public void ctor_FieldMessage_shouldPass() {

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.message, regular);
    }

    @Test
    public void apply_validDeviceName_shouldReturnTrue() {

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.device, String.format(regular, DEVICE_NAME));

        LogEntryEntity logEntry = new LogEntryEntity(DATE_TIME, DEVICE_NAME_VALID, MESSAGE_TYPE, MESSAGE);
        boolean result = filter.apply(logEntry);
        Assert.assertTrue(result);
    }

    @Test
    public void apply_validMessage_shouldReturnTrue() {

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.message, String.format(regular, MESSAGE));

        LogEntryEntity logEntry = new LogEntryEntity(DATE_TIME, DEVICE_NAME, MESSAGE_TYPE, MESSAGE_VALID);
        boolean result = filter.apply(logEntry);
        Assert.assertTrue(result);
    }

    @Test
    public void apply_invalidDeviceName_shouldReturnFalse() {

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.device, String.format(regular, DEVICE_NAME));

        LogEntryEntity logEntry = new LogEntryEntity(DATE_TIME, DEVICE_NAME, MESSAGE_TYPE, MESSAGE);
        boolean result = filter.apply(logEntry);
        Assert.assertFalse(result);
    }

    @Test
    public void apply_invalidMessage_shouldReturnFalse() {

        MatchBinaryFilter filter = new MatchBinaryFilter(LogEntryFieldType.message, String.format(regular, MESSAGE));

        LogEntryEntity logEntry = new LogEntryEntity(DATE_TIME, DEVICE_NAME, MESSAGE_TYPE, MESSAGE);
        boolean result = filter.apply(logEntry);
        Assert.assertFalse(result);
    }

}
