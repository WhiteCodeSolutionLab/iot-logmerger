/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure.filters;

import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.entities.filters.LogEntryFieldType;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDateTime;

public class LessEqualBinaryFilterTests {
    private final static LocalDateTime DATE_TIME = LocalDateTime.of(2015, 12, 6, 0, 0);
    private final static LocalDateTime LESS_DATE_TIME = LocalDateTime.of(2015, 12, 2, 0, 0);
    private final static LocalDateTime GREATER_DATE_TIME = LocalDateTime.of(2015, 12, 24, 0, 0);

    private final static String DEVICE_NAME = "Device";

    private final static String MESSAGE_TYPE = "Type";

    private final static String MESSAGE = "Message";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void ctor_nullFiledTypeArgument_shouldThrownException() {
        thrown.expect(IllegalArgumentException.class);
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(null, DATE_TIME);
    }

    @Test
    public void ctor_nullFiledValueArgument_shouldThrownException() {
        thrown.expect(IllegalArgumentException.class);
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.timestamp, null);
    }

    @Test
    public void ctor_unsupportedFieldDeviceName_shouldThrownException() {
        thrown.expect(UnsupportedOperationException.class);
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.device, DEVICE_NAME);
    }

    @Test
    public void ctor_unsupportedFieldMessageType_shouldThrownException() {
        thrown.expect(UnsupportedOperationException.class);
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.type, MESSAGE_TYPE);
    }

    @Test
    public void ctor_unsupportedFieldMessage_shouldThrownException() {
        thrown.expect(UnsupportedOperationException.class);
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.message, MESSAGE);
    }

    @Test
    public void apply_lessDate_shouldReturnTrue() {
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.timestamp, DATE_TIME);

        LogEntryEntity entryEntity = new LogEntryEntity(LESS_DATE_TIME, DEVICE_NAME, MESSAGE_TYPE, MESSAGE);

        boolean result = filter.apply(entryEntity);
        Assert.assertTrue(result);
    }

    @Test
    public void apply_equalDates_shouldReturnTrue() {
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.timestamp, DATE_TIME);

        LogEntryEntity entryEntity = new LogEntryEntity(DATE_TIME, DEVICE_NAME, MESSAGE_TYPE, MESSAGE);

        boolean result = filter.apply(entryEntity);
        Assert.assertTrue(result);
    }

    @Test
    public void apply_greaterDate_shouldReturnFalse() {
        LessEqualBinaryFilter filter = new LessEqualBinaryFilter(LogEntryFieldType.timestamp, DATE_TIME);

        LogEntryEntity entryEntity = new LogEntryEntity(GREATER_DATE_TIME, DEVICE_NAME, MESSAGE_TYPE, MESSAGE);

        boolean result = filter.apply(entryEntity);
        Assert.assertFalse(result);
    }
}
