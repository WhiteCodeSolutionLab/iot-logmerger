/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger;

import com.whitecode.iot.logmerger.entities.*;
import com.whitecode.iot.logmerger.exceptions.*;
import com.whitecode.iot.logmerger.infrastructure.*;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("IoT-LogMerger v1.0, by WhiteCode team 2015");

            if (args == null || args.length < 1) {
                System.err.println("Invalid argument. Please specify a configuration file name");
                Views.printHelp();
                return;
            }

            // Read configuration file
            FileSettingsProvider settingProvider = ApplicationImpl.readConfig(args[0]);

            // Validate configuration settings
            ApplicationImpl.validateConfigSettings(settingProvider);

            //Set thread pool size
            ThreadPoolManager.setThreadPoolSize(settingProvider.getThreadPoolSize());

            // Display selected configuration
            Views.printSelectedConfig(settingProvider);

            // Fetch source logs
            List<InputLogFileEntity> sourceLogs = ApplicationImpl.fetchLogsFromSources(settingProvider);
            Views.printSourceLogs(sourceLogs);

            //Processing
            System.out.println("Start merge process");
            long time = ApplicationImpl.logMerging(sourceLogs, settingProvider);

            // Print results information
            Views.printResult(time, settingProvider);
        } catch (FetchFailedException e) {
            if(e.getNode().isEmpty()) {
                System.err.printf("Exception while fetching with %s adapter:\n%s",
                        e.getAdapterName(),
                        e.getMessage());
            } else {
                System.err.printf("Exception while fetching from %s with %s adapter:\n%s\n",
                        e.getNode(),
                        e.getAdapterName(),
                        e.getMessage());
            }
        } catch (UserException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Unexpected exception in application!\n");
            e.printStackTrace();
        } finally {
            ThreadPoolManager.shutdownExecutorService();
        }
    }
}
