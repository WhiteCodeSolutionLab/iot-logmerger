/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Manage executor service.
 * @version 1.1
 */
public class ThreadPoolManager {
    private static int threadPoolSize;

    public static void setThreadPoolSize(int settingThreadPoolSize){
        threadPoolSize = settingThreadPoolSize;
    }

    private static ExecutorService ourInstance = null;

    private static ExecutorService getInstance() {
        if (ourInstance == null) {
            ourInstance = Executors.newFixedThreadPool(threadPoolSize);
        }
        return ourInstance;
    }

    private ThreadPoolManager() {
    }

    public static Future submit(Runnable task){
        if(task == null){
            throw new ArgumentNullException("task");
        }
        return getInstance().submit(task);
    }

    public static Future submit(Callable task){
        if(task == null){
            throw new ArgumentNullException("task");
        }
        return getInstance().submit(task);
    }

    /**
     * Shut down static executor service.
     */
    public static void shutdownExecutorService() {
        if(ourInstance != null){
            ourInstance.shutdown();
        }
    }
}
