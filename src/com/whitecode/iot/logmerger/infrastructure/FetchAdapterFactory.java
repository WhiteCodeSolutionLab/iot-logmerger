/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.datalayer.interfaces.IFetchAdapter;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.interfaces.IFetchAdapterFactory;

import java.util.Hashtable;
import java.util.Map;
import java.util.function.Function;

/**
 * Implements a fetch adapter factory.
 * @version 1.1
 */
public class FetchAdapterFactory implements IFetchAdapterFactory {

    public static final String ADAPTER_ALREADY_REGISTERED_EXCEPTION_MESSAGE_FORMAT = "Adapter '%s' already registered";
    private Map<String, Function<String, IFetchAdapter>> adapterFunctionMap;

    private Function<String, IFetchAdapter> defaultAdapterFunction;

    /**
     * Sets a default adapter function for creating a new instance of adapter.
     * @param adapterFunction The adapter creation function.
     */
    public void setDefaultAdapterFunction(Function<String, IFetchAdapter> adapterFunction) {
        defaultAdapterFunction = adapterFunction;
    }

    /**
     * Register an adapter function for creating a new instance of adapter.
     * @param adapterName The adapter name.
     * @param adapterFunction The adapter creation function.
     */
    public void registerAdapter(String adapterName, Function<String, IFetchAdapter> adapterFunction) {
        if (adapterName == null || adapterName.length() == 0) {
            throw new ArgumentNullException("adapterName");
        }

        if (adapterFunction == null) {
            throw new ArgumentNullException("adapterFunction");
        }

        if(adapterFunctionMap == null) {
            adapterFunctionMap = new Hashtable<>();
        }

        if (adapterFunctionMap.containsKey(adapterName)) {
            throw new IllegalArgumentException(String.format(ADAPTER_ALREADY_REGISTERED_EXCEPTION_MESSAGE_FORMAT, adapterName));
        }

        adapterFunctionMap.put(adapterName, adapterFunction);
    }

    /**
     * Create a new instance of the adapter.
     * @param adapterName The fetch adapter name.
     * @param fetchContext The fetch context.
     * @return The instance of the fetch adapter.
     */
    @Override
    public IFetchAdapter createAdapter(String adapterName, String fetchContext) {
        if (adapterName != null && !adapterName.isEmpty()) {
            if (adapterFunctionMap.containsKey(adapterName)) {
                Function<String, IFetchAdapter> adapterFunction = adapterFunctionMap.get(adapterName);
                return adapterFunction.apply(fetchContext);
            } else {
                throw new IllegalArgumentException(String.format("Adapter '%s' not supported", adapterName));
            }
        }

        // Gets a default adapter if possible.
        if (defaultAdapterFunction == null) {
            throw new IllegalArgumentException("Default adapter is not supported");
        }

        return defaultAdapterFunction.apply(fetchContext);
    }
}
