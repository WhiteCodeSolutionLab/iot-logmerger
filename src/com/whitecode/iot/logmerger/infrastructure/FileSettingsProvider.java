/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.entities.SourceSettingsEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.interfaces.IFetcherSettingsProvider;
import com.whitecode.iot.logmerger.entities.filters.*;
import com.whitecode.iot.logmerger.infrastructure.interfaces.ISettingsProvider;

import org.json.*;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Implements a settings provider with reader from JSON configuration file.
 *
 * @version 1.1
 */
public class FileSettingsProvider implements ISettingsProvider, IFetcherSettingsProvider {
    // Exception messages
    private static final String WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE = "Config file has wrong file format";
    private static final String KEY_NOT_FOUND_IN_ENTITY_EXCEPTION_MESSAGE = WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE + "\n\"%s\" key not found in configuration file";
    private static final String KEY_NOT_FOUND_IN_JSON_ENTITY_EXCEPTION_MESSAGE = WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE + "\n\"%s\" key not exists in entity: %s";
    private static final String WRONG_TIME_OFFSET_EXCEPTION_MESSAGE = "Time offset has wrong format";

    // Log file keys
    private static final String SOURCE_LOG_FILES_JSON_KEY = "sourceLogFiles";
    private static final String FILE_PATH_JSON_ENTITY_KEY = "filePath";
    private static final String FORMAT_JSON_ENTITY_KEY = "format";
    private static final String TIME_OFFSET_JSON_ENTITY_KEY = "timeOffset";
    private static final String TARGET_LOG_FILE_PATH_JSON_KEY = "targetLogFilePath";
    private static final String WORK_DIRECTORY_PATH_JSON_KEY = "workDirectoryPath";
    private static final String FETCH_IGNORE_ERRORS_JSON_KEY = "fetchIgnoreErrors";
    private static final String ADAPTER_NAME_JSON_ENTITY_KEY = "adapter";
    private static final String ADAPTER_CONTEXT_JSON_ENTITY_KEY = "adapterContext";
    private static final int DEFAULT_READER_MAX_BUFFER_SIZE = 1000;
    private static final int DEFAULT_WRITER_MAX_BUFFER_SIZE = 1000;
    public static final int DEFAULT_TREAD_POOL_SIZE_VALUE = 15;
    public static final String FILTER_TYPE_JSON_KEY = "type";
    public static final String FILTER_VALUE_JSON_KEY = "value";
    public static final String FILTER_FIELDS_JSON_KEY = "field";
    public static final String FILTER_FILTERS_JSON_KEY = "filters";
    public static final String FILTER_JSON_KEY = "filter";


    /**
     * The path to target log file.
     */
    private String targetFilePath;

    /**
     * Gets a path to target log file.
     *
     * @return The file path.
     */
    public String getTargetLogFilePath() {
        return targetFilePath;
    }

    /**
     * The path to directory in which fetched files will be stored.
     */
    private String workDirectoryPath;

    /**
     * Gets a path to work directory.
     *
     * @return The directory path.
     */
    public String getWorkDirectoryPath() {
        return workDirectoryPath;
    }

    /**
     * List of the source services settings.
     */
    private List<SourceSettingsEntity> sourceSettingsEntities;

    /**
     * If true fetcher will try to ignore errors that can appear during fetch process.
     */
    private boolean fetchIgnoreErrors;

    /**
     * Checks if fetcher should ignore errors.
     *
     * @return the fetcherIgnoreErrors param value.
     */
    public boolean isFetchIgnoreErrors() {
        return fetchIgnoreErrors;
    }


    /**
     * Gets a information about source services which provides a log files.
     *
     * @return List of the source services settings.
     */
    @Override
    public List<SourceSettingsEntity> getSourceSettingsEntities() {
        return sourceSettingsEntities;
    }

    private int readerMaxBufferSize;

    @Override
    public int getReaderMaxBufferSize() {
        return readerMaxBufferSize;
    }

    private int writerMaxBufferSize;

    @Override
    public int getWriterMaxBufferSize() {
        return writerMaxBufferSize;
    }

    private int threadPoolSize;

    @Override
    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    private BaseFilterEntity filter;

    @Override
    public BaseFilterEntity getFilter() {
        return filter;
    }

    /**
     * Creates a new instance of the file settings provider.
     *
     * @param filePath Path to the file which contains application settings.
     * @throws IOException
     * @throws IllegalArgumentException
     */
    public FileSettingsProvider(String filePath) throws IOException, IllegalArgumentException {
        if (filePath == null || filePath.isEmpty()) {
            throw new ArgumentNullException("filePath");
        }

        JSONObject jsonObject = readConfigFile(filePath);
        initializeSettings(jsonObject);
    }

    public FileSettingsProvider(JSONObject jsonObject) throws IOException {
        initializeSettings(jsonObject);
    }

    private void initializeSettings(JSONObject jsonObject) throws IOException {
        if (jsonObject == null) {
            throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE);
        }

        workDirectoryPath = readWorkDirectoryPath(jsonObject);
        fetchIgnoreErrors = readFetchIgnoreErrorsParam(jsonObject);

        targetFilePath = readTargetFilePath(jsonObject);
        sourceSettingsEntities = readSourceSettingsEntities(jsonObject);

        readerMaxBufferSize = readIntField(jsonObject, "readerMaxBufferSize", DEFAULT_READER_MAX_BUFFER_SIZE);
        if (readerMaxBufferSize <= 0) {
            throw new IllegalArgumentException(String.format("Parameter '%s' should be greater that zero", readerMaxBufferSize));
        }

        writerMaxBufferSize = readIntField(jsonObject, "writerMaxBufferSize", DEFAULT_WRITER_MAX_BUFFER_SIZE);
        if (writerMaxBufferSize <= 0) {
            throw new IllegalArgumentException(String.format("Parameter '%s' should be greater that zero", writerMaxBufferSize));
        }

        threadPoolSize = readIntField(jsonObject, "threadPoolSize", DEFAULT_TREAD_POOL_SIZE_VALUE);

        filter = readFilterEntity(jsonObject);
    }

    private JSONObject readConfigFile(String filePath) throws IOException, IllegalArgumentException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        StringBuilder text = new StringBuilder();
        String line = reader.readLine();
        while (line != null) {
            text.append(line);
            text.append("\n");
            line = reader.readLine();
        }

        try {
            return new JSONObject(text.toString());
        } catch (JSONException exception) {
            throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE, exception);
        }
    }

    private String readTargetFilePath(JSONObject jsonObject) {
        if (!jsonObject.has(TARGET_LOG_FILE_PATH_JSON_KEY)) {
            throw new IllegalArgumentException(String.format(KEY_NOT_FOUND_IN_ENTITY_EXCEPTION_MESSAGE, TARGET_LOG_FILE_PATH_JSON_KEY));
        }

        try {
            return jsonObject.getString(TARGET_LOG_FILE_PATH_JSON_KEY);
        } catch (JSONException exception) {
            throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE, exception);
        }
    }

    private boolean readFetchIgnoreErrorsParam(JSONObject jsonObject) {
        return jsonObject.has(FETCH_IGNORE_ERRORS_JSON_KEY) && jsonObject.getBoolean(FETCH_IGNORE_ERRORS_JSON_KEY);

    }

    private String readWorkDirectoryPath(JSONObject jsonObject) throws SecurityException, IOException {
        String workPath = null;
        File workFolder;

        if (jsonObject.has(WORK_DIRECTORY_PATH_JSON_KEY)) {
            workPath = jsonObject.getString(WORK_DIRECTORY_PATH_JSON_KEY);

            if (workPath.isEmpty()) {
                workPath = null; //Consider empty work path as default
            } else {
                workFolder = new File(workPath);
                if (workFolder.isFile()) {
                    workPath = null; //Sadly but we don't know how to distinguish a file without extension and a folder
                } else if (!workFolder.mkdirs()) {
                    if (!workFolder.exists()) {
                        workPath = null; //Folder doesn't exist but cannot be created for some reason
                    }
                }
            }
        }

        if (workPath == null) {
            workPath = System.getProperty("user.dir") + File.separator + "tmp" + File.separator;
            workFolder = new File(workPath);
            if (workFolder.isFile()) {
                throw new IOException("There's a file named as the default work folder! Delete the file or set a custom work folder.");
            } else if (!workFolder.mkdirs()) {
                if (!workFolder.exists()) {
                    throw new IOException("Default work folder does not exist and cannot be created!");
                }
            }
        } else if (!workPath.endsWith(File.separator)) {
            workPath = workPath + File.separator;
        }

        return workPath;
    }

    private int readIntField(JSONObject jsonObject, String fieldName, int defaultValue) {
        int intField;
        if (!jsonObject.has(fieldName)) {
           intField = defaultValue;
        } else {
            try {
                intField = jsonObject.getInt(fieldName);
            } catch (JSONException exception) {
                throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE, exception);
            }
        }

        return intField;
    }

    private SourceSettingsEntity parseSourceSettingsEntity(JSONObject jsonObject) {
        if (!jsonObject.has(FILE_PATH_JSON_ENTITY_KEY)) {
            throw new IllegalArgumentException(String.format(KEY_NOT_FOUND_IN_JSON_ENTITY_EXCEPTION_MESSAGE, FILE_PATH_JSON_ENTITY_KEY, jsonObject.toString()));
        }

        if (!jsonObject.has(FORMAT_JSON_ENTITY_KEY)) {
            throw new IllegalArgumentException(String.format(KEY_NOT_FOUND_IN_JSON_ENTITY_EXCEPTION_MESSAGE, FORMAT_JSON_ENTITY_KEY, jsonObject.toString()));
        }

        List<String> fileMasks = new ArrayList<>();
        Object filePathObject = jsonObject.get(FILE_PATH_JSON_ENTITY_KEY);
        if (filePathObject instanceof String) {
            fileMasks.add((String) filePathObject);
        } else {
            if (filePathObject instanceof JSONArray) {
                JSONArray filePathList = (JSONArray) filePathObject;
                for (int i = 0; i < filePathList.length(); i++) {
                    fileMasks.add(filePathList.getString(i));
                }
            } else {
                throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE);
            }
        }

        String formatName = jsonObject.getString(FORMAT_JSON_ENTITY_KEY);

        String adapterName = null;
        if (jsonObject.has(ADAPTER_NAME_JSON_ENTITY_KEY)) {
            adapterName = jsonObject.getString(ADAPTER_NAME_JSON_ENTITY_KEY);
        }

        String adapterContext = null;
        if (jsonObject.has(ADAPTER_CONTEXT_JSON_ENTITY_KEY)) {
            JSONObject adapterContextObject = jsonObject.getJSONObject(ADAPTER_CONTEXT_JSON_ENTITY_KEY);
            adapterContext = adapterContextObject.toString();
        }

        boolean timeOffsetSign = false;
        LocalDateTime timeOffset = null;

        if (jsonObject.has(TIME_OFFSET_JSON_ENTITY_KEY)) {
            String timeOffsetString = jsonObject.getString(TIME_OFFSET_JSON_ENTITY_KEY);
            if (timeOffsetString.charAt(0) == '+') {
                timeOffsetSign = true;
            } else if (timeOffsetString.charAt(0) == '-') {
                timeOffsetSign = false;
            } else {
                throw new IllegalArgumentException(WRONG_TIME_OFFSET_EXCEPTION_MESSAGE);
            }
            timeOffsetString = timeOffsetString.substring(1);
            timeOffset = LocalDateTime.parse(timeOffsetString);
        }

        return new SourceSettingsEntity(fileMasks, formatName, adapterName, adapterContext, timeOffset, timeOffsetSign);
    }

    private List<SourceSettingsEntity> readSourceSettingsEntities(JSONObject jsonObject) {
        if (!jsonObject.has(SOURCE_LOG_FILES_JSON_KEY)) {
            throw new IllegalArgumentException(String.format(KEY_NOT_FOUND_IN_ENTITY_EXCEPTION_MESSAGE, SOURCE_LOG_FILES_JSON_KEY));
        }

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(SOURCE_LOG_FILES_JSON_KEY);
            if (jsonArray == null) {
                throw new IllegalArgumentException(String.format(KEY_NOT_FOUND_IN_ENTITY_EXCEPTION_MESSAGE, SOURCE_LOG_FILES_JSON_KEY));
            }

            List<SourceSettingsEntity> list = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                SourceSettingsEntity sourceSettingsEntity = parseSourceSettingsEntity(item);
                list.add(sourceSettingsEntity);
            }

            return list;
        } catch (JSONException exception) {
            throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE, exception);
        }
    }

    private BaseFilterEntity readFilterEntity(JSONObject jsonObject) {
        if (!jsonObject.has(FILTER_JSON_KEY)) {
            return null;
        }

        JSONObject filter = jsonObject.getJSONObject(FILTER_JSON_KEY);
        return parseFilterEntity(filter);
    }

    private BaseFilterEntity parseFilterEntity(JSONObject filter) {
        if (filter.has(FILTER_TYPE_JSON_KEY)) {
            if (filter.has(FILTER_FILTERS_JSON_KEY)) {
                return getComplexFilterEntity(filter);
            } else if (filter.has(FILTER_FIELDS_JSON_KEY) && filter.has(FILTER_VALUE_JSON_KEY)) {
                return getBaseBinaryFilterEntity(filter);
            } else {
                throw new IllegalArgumentException(WRONG_CONFIG_FILE_FORMAT_EXCEPTION_MESSAGE);
            }
        }
        return null;
    }

    private ComplexFilterEntity getComplexFilterEntity(JSONObject filter) {
        ComplexFilterOperationType type = ComplexFilterOperationType.valueOf(filter.getString(FILTER_TYPE_JSON_KEY));
        List<BaseFilterEntity> filters = new ArrayList<>();
        JSONArray jsonArray = filter.getJSONArray(FILTER_FILTERS_JSON_KEY);
        for (int i = 0; i < jsonArray.length(); i++) {
            filters.add(parseFilterEntity(jsonArray.getJSONObject(i)));
        }
        return new ComplexFilterEntity(type, filters);
    }

    private BaseBinaryFilterEntity getBaseBinaryFilterEntity(JSONObject filter) {
        BinaryFilterOperationType type = BinaryFilterOperationType.valueOf(filter.getString(FILTER_TYPE_JSON_KEY));
        LogEntryFieldType fieldType = LogEntryFieldType.valueOf(filter.getString(FILTER_FIELDS_JSON_KEY));

        Object objectValue = filter.get(FILTER_VALUE_JSON_KEY);

        if (objectValue instanceof String) {
            return new SimpleBinaryFilterEntity(type, fieldType, objectValue.toString());
        }

        if (objectValue instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) objectValue;
            List<String> valueList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                valueList.add(jsonArray.getString(i));
            }
            return new ComplexBinaryFilterEntity(type, fieldType, valueList);
        }

        return null;
    }
}