/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.entities.filters.*;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.filters.*;
import com.whitecode.iot.logmerger.infrastructure.interfaces.IFilterFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements a method for building a filters.
 * @version 1.2
 */
public class FilterFactory implements IFilterFactory {
    private static final String UNSUPPORTED_ERROR_MESSAGE = "'%s' is not supported";

    /**
     * Create a new instance of filter from base filter entity.
     * @param filterEntity The instance with filter configurations.
     * @return The instance of filter implementation.
     */
    public BaseFilter buildFilter(BaseFilterEntity filterEntity) {
        if (filterEntity == null) {
            throw new ArgumentNullException("filterEntity");
        }

        if (filterEntity instanceof ComplexFilterEntity) {
            return buildComplexFilter((ComplexFilterEntity) filterEntity);
        }

        if (filterEntity instanceof BaseBinaryFilterEntity){
            return buildBinaryFilter((BaseBinaryFilterEntity)filterEntity);
        }

        throw new UnsupportedOperationException(String.format(UNSUPPORTED_ERROR_MESSAGE, filterEntity.getClass().getName()));
    }

    private BaseComplexFilter buildComplexFilter(ComplexFilterEntity complexFilterEntity){
        List<BaseFilter> filters = new ArrayList<>();
        for(BaseFilterEntity filter: complexFilterEntity.getFilters()){
            filters.add(buildFilter(filter));
        }

        switch (complexFilterEntity.getOperationType()){
            case and:
                return new AndComplexFilter(filters);
            case or:
                return new OrComplexFilter(filters);
            default:
                throw new UnsupportedOperationException(String.format(UNSUPPORTED_ERROR_MESSAGE, complexFilterEntity.getOperationType()));
        }
    }

    private BaseBinaryFilter buildBinaryFilter(BaseBinaryFilterEntity binaryFilterEntity){
        Object value = getBinaryFilterValue(binaryFilterEntity);
        LogEntryFieldType fieldType = binaryFilterEntity.getFieldType();

        switch(binaryFilterEntity.getOperationType()){
            case greater:
                return new GreaterBinaryFilter(fieldType, value);
            case greaterOrEqual:
                return new GreaterEqualBinaryFilter(fieldType, value);
            case equal:
                return new EqualBinaryFilter(fieldType, value);
            case notEqual:
                return new NotEqualBinaryFilter(fieldType, value);
            case lessOrEqual:
                return new LessEqualBinaryFilter(fieldType, value);
            case less:
                return new LessBinaryFilter(fieldType, value);
            case match:
                return new MatchBinaryFilter(fieldType, value);
            default:
                throw new UnsupportedOperationException(String.format(UNSUPPORTED_ERROR_MESSAGE, fieldType));
        }
    }

    private Object getBinaryFilterValue(BaseBinaryFilterEntity binaryFilterEntity){
        if(binaryFilterEntity instanceof SimpleBinaryFilterEntity){
            return  ((SimpleBinaryFilterEntity) binaryFilterEntity).getValue();
        } else if(binaryFilterEntity instanceof ComplexBinaryFilterEntity){
            return  ((ComplexBinaryFilterEntity) binaryFilterEntity).getValues();
        } else{
            throw new UnsupportedOperationException(String.format(UNSUPPORTED_ERROR_MESSAGE, binaryFilterEntity.getClass().getName()));
        }
    }
}
