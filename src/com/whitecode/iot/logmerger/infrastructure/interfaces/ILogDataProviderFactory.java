/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure.interfaces;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogReader;
import com.whitecode.iot.logmerger.datalayer.interfaces.ILogWriter;
import com.whitecode.iot.logmerger.entities.InputLogFileEntity;

import java.io.FileNotFoundException;

/**
 * Provides methods for creating a log data providers (log reader or log writer).
 * @version 1.0
 */
public interface ILogDataProviderFactory {
    /**
     * Creates a new log reader.
     *
     * @param inputLogFileEntity The input log file entity.
     * @return The log reader.
     * @throws FileNotFoundException
     */
    ILogReader getLogReader(InputLogFileEntity inputLogFileEntity) throws FileNotFoundException;

    /**
     * Creates a new log writer.
     *
     * @param targetPath The name of output file.
     * @return The log writer.
     * @throws FileNotFoundException
     */
    ILogWriter getLogWriter(String targetPath) throws FileNotFoundException;
}
