/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.datalayer.*;
import com.whitecode.iot.logmerger.datalayer.interfaces.ILogReader;
import com.whitecode.iot.logmerger.datalayer.interfaces.ILogWriter;
import com.whitecode.iot.logmerger.entities.InputLogFileEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.interfaces.ILogDataProviderFactory;
import com.whitecode.iot.logmerger.infrastructure.interfaces.ISettingsProvider;

import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.Hashtable;
import java.util.Map;
import java.util.function.Function;

/**
 * Implements a log data providers factory.
 *
 * @version 1.0
 */
public class LogDataProviderFactory implements ILogDataProviderFactory {
    private Map<String, Function<InputLogFileEntity, ILogReader>> logReaderMapping = new Hashtable<>();

    private ISettingsProvider settingProvider;

    public void registerReader(String formatName, Function<InputLogFileEntity, ILogReader> readerFunction) {
        if (formatName == null || formatName.isEmpty()) {
            throw new ArgumentNullException("formatName");
        }

        if (readerFunction == null) {
            throw new ArgumentNullException("readerFunction");
        }

        if (logReaderMapping.containsKey(formatName)) {
            throw new IllegalArgumentException(String.format("Log reader for \"%s\" already registered", formatName));
        }

        logReaderMapping.put(formatName, readerFunction);
    }

    public LogDataProviderFactory(ISettingsProvider settingProvider) {
        if (settingProvider == null) {
            throw new ArgumentNullException("settingProvider");
        }

        this.settingProvider = settingProvider;
    }

    private ILogReader getBaseLogReader(InputLogFileEntity inputLogFileEntity) {
        String formatName = inputLogFileEntity.getLogFormatType();

        if (!logReaderMapping.containsKey(formatName)) {
            throw new IllegalArgumentException(String.format("Log reader not found for format with name \"%s\"", formatName));
        }

        Function<InputLogFileEntity, ILogReader> readerFunction = logReaderMapping.get(formatName);

        return readerFunction.apply(inputLogFileEntity);
    }

    /**
     * Creates a new log reader.
     *
     * @param inputLogFileEntity The input log file entity.
     * @return The log reader.
     */
    @Override
    public ILogReader getLogReader(InputLogFileEntity inputLogFileEntity) {
        if (inputLogFileEntity == null) {
            throw new ArgumentNullException("inputLogFileEntity");
        }

        ILogReader baseLogReader = getBaseLogReader(inputLogFileEntity);
        if (baseLogReader == null) {
            throw new NullPointerException(String.format("Factory can't create a new instance of the ILogReader for format \"%s\"", inputLogFileEntity.getLogFormatType()));
        }

        if (inputLogFileEntity.getTimeOffset() != null) {
            baseLogReader = new TimeOffsetLogReader(inputLogFileEntity.getTimeOffset(),
                    inputLogFileEntity.getTimeOffsetSign(),
                    baseLogReader);
        }

        return new BufferedLogReader(baseLogReader, settingProvider.getReaderMaxBufferSize());

    }

    /**
     * Creates a new log writer.
     *
     * @param outputStream The output stream.
     * @return The log writer in new thread.
     */
    public ILogWriter getLogWriter(OutputStream outputStream) {
        if (outputStream == null) {
            throw new ArgumentNullException("outputStream");
        }

        StringLogFormatter formatter = new StringLogFormatter(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS"));
        StreamLogWriter streamLogWriter = new StreamLogWriter(outputStream, formatter);

        return new BufferedLogWriter(streamLogWriter, settingProvider.getWriterMaxBufferSize());
    }

    /**
     * Creates a new log writer.
     *
     * @param targetPath The name of output file.
     * @return The log writer.
     * @throws FileNotFoundException
     */
    @Override
    public ILogWriter getLogWriter(String targetPath) throws FileNotFoundException {
        if ((targetPath == null) || (targetPath.isEmpty())) {
            throw new ArgumentNullException("targetPath");
        }

        OutputStream outputStream = new FileOutputStream(targetPath);
        return getLogWriter(outputStream);
    }
}
