/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure.filters;

import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.entities.filters.BinaryFilterOperationType;
import com.whitecode.iot.logmerger.entities.filters.LogEntryFieldType;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implements a regex filtration for log entry fields.
 * Supported only for "device", "message" fields in log entity.
 * @version 1.2
 */
public class MatchBinaryFilter extends BaseBinaryFilter {

    private static final String UNSUPPORTED_ERROR_MESSAGE = "'%s' is not supported";

    private Pattern pattern;

    public MatchBinaryFilter(LogEntryFieldType field, Object value) {
        super(field, value, BinaryFilterOperationType.match);
        switch (field) {
            case device:
            case message:
                break;
            default:
                throw new UnsupportedOperationException(String.format(UNSUPPORTED_ERROR_MESSAGE, field));
        }
        pattern = Pattern.compile(value.toString());
    }

    @Override
    public boolean apply(LogEntryEntity logEntry) {
        if (logEntry == null) {
            throw new ArgumentNullException("logEntry");
        }

        switch(field){
            case device:
                return match(logEntry.getDeviceName());
            case message:
                return match(logEntry.getMessage());
            default:
                throw new UnsupportedOperationException(String.format(UNSUPPORTED_ERROR_MESSAGE, field));
        }
    }

    private boolean match(String str){
        Matcher match = pattern.matcher(str);
        return match.matches();
    }
}
