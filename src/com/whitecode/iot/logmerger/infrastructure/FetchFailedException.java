/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

/**
 * A class representing exceptions that can be thrown during fetching.
 * @version 1.1
 */
public class FetchFailedException extends Exception {
    /**
     * URL or IP-address of the host that caused the exception.
     */
    private String node;

    /**
     * A type of adapter that threw the exception.
     */
    private String adapterName;

    /**
     * Another exception that caused a FetchFailedException instance.
     */
    private Throwable cause;

    /**
     * Create a new instance of the exception.
     * @param message The exception message.
     * @param node URL or IP-address of the node that caused the exception.
     * @param adapterName the name of an adapter that threw the exception.
     */
    public FetchFailedException(String message, String node, String adapterName) {
        super(message);
        this.node = node;
        this.adapterName = adapterName;
    }

    /**
     * Create a new instance of the exception.
     * @param message The exception message.
     * @param node URL or IP-address of the node that caused the exception.
     * @param adapterName the name of an adapter that threw the exception.
     */
    public FetchFailedException(String message, String node, String adapterName, Throwable cause) {
        super(message);
        this.node = node;
        this.adapterName = adapterName;
        this.cause = cause;
    }


    /**
     * Getter-method for the node that caused the exception.
     * @return the URL of IP-address.
     */
    public String getNode() {
        return node;
    }

    /**
     * Getter-method for the type of adapter that threw the exception.
     * @return fetch-adapter name (e.g. SSH or FTP).
     */
    public String getAdapterName() {
        return adapterName;
    }

    /**
     * Getter-method for the exception that caused a FetchFailedException instance.
     * @return a Throwable object.
     */
    public Throwable getCause() {
        return cause;
    }
}
