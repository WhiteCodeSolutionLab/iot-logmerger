/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogReader;
import com.whitecode.iot.logmerger.datalayer.interfaces.ILogWriter;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.infrastructure.filters.BaseFilter;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.interfaces.ILogProcessorComponent;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implements a log processor component.
 * @version 1.2
 */
public class LogProcessorComponent implements ILogProcessorComponent {

    private BaseFilter filter;

    public BaseFilter getFilter() {
        return filter;
    }

    public void setFilter(BaseFilter filter) {
        this.filter = filter;
    }

    /**
     * Method that implements merging of all inputs into one output.
     * @param logReaders - list of log readers that provide input data.
     * @param logWriter - object responsible for writing output.
     */
     public void Merge(List<ILogReader> logReaders, ILogWriter logWriter) throws IOException {
         if (logReaders == null) {
             throw new ArgumentNullException("logReaders");
         }

         if (logWriter == null) {
             throw new ArgumentNullException("logWriter");
         }

         try {
            ArrayList<LogEntryEntity> competitors = new ArrayList<>(logReaders.size());
            LogEntryEntity minEntity;
            LogEntryEntity nextEntity;
            int idxMinEntity;

            for (int i = 0; i < logReaders.size(); i++) {
                nextEntity = logReaders.get(i).ReadEntry();
                if(nextEntity != null) {
                    competitors.add(nextEntity);
                } else {
                    logReaders.remove(i--);
                }
            }

            while(!competitors.isEmpty()) {
                minEntity = Collections.min(competitors);
                if(isPassed(minEntity)){
                    logWriter.WriteEntry(minEntity);
                }

                idxMinEntity = competitors.indexOf(minEntity);
                nextEntity = logReaders.get(idxMinEntity).ReadEntry();

                if (nextEntity != null) {
                    competitors.set(idxMinEntity, nextEntity);
                } else {
                    competitors.remove(idxMinEntity);
                    logReaders.remove(idxMinEntity);
                }
            }
        } catch (IOException e) {
            throw e;
            //TODO: implement catch-er
        } finally {
            List<Closeable> streamList = new ArrayList<>();
            streamList.addAll(logReaders);
            streamList.add(logWriter);
            for (Closeable stream : streamList) {
                stream.close();
            }
        }
    }

    private boolean isPassed(LogEntryEntity entryEntity) {
        return getFilter() == null || getFilter().apply(entryEntity);
    }
}
