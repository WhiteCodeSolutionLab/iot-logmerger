/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.infrastructure;

import com.whitecode.iot.logmerger.datalayer.interfaces.IFetchAdapter;
import com.whitecode.iot.logmerger.entities.InputLogFileEntity;
import com.whitecode.iot.logmerger.entities.SourceSettingsEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.interfaces.IFetchAdapterFactory;
import com.whitecode.iot.logmerger.infrastructure.interfaces.IFetchProcessorComponent;
import com.whitecode.iot.logmerger.infrastructure.interfaces.IFetcherSettingsProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implements a fetch processor component.
 * @version 1.1
 */
public class FetchProcessorComponent implements IFetchProcessorComponent {
    private IFetchAdapterFactory fetchAdapterFactory;

    private String baseWorkingDirectoryPath;
    private boolean isIgnoreErrors;

    protected String getBaseWorkingDirectoryPath() {
        return baseWorkingDirectoryPath;
    }

    protected boolean isIgnoreErrors() {
        return isIgnoreErrors;
    }

    protected IFetchAdapterFactory getFetchAdapterFactory() {
        return fetchAdapterFactory;
    }

    public FetchProcessorComponent(IFetchAdapterFactory fetchAdapterFactory, IFetcherSettingsProvider fetchSettingsProvider) {
        this.fetchAdapterFactory = fetchAdapterFactory;

        this.baseWorkingDirectoryPath = fetchSettingsProvider.getWorkDirectoryPath();
        this.isIgnoreErrors = fetchSettingsProvider.isFetchIgnoreErrors();
    }

    private List<InputLogFileEntity> convertLocalFilesToEntity(List<String> localFiles, SourceSettingsEntity sourceSettingsEntity) {
        // Converting
        List<InputLogFileEntity> resultEntities = new ArrayList<>();
        for(String localFile : localFiles) {
            resultEntities.add(new InputLogFileEntity(localFile,
                    sourceSettingsEntity.getLogFormatType(),
                    sourceSettingsEntity.getTimeOffset(),
                    sourceSettingsEntity.getTimeOffsetSign()));
        }

        return resultEntities;
    }

    private List<InputLogFileEntity> fetch(SourceSettingsEntity sourceSettings, String workPath) throws FetchFailedException {
        if (sourceSettings == null) {
            throw new ArgumentNullException("sourceSettings");
        }

        // Create adapter for fetching
        IFetchAdapter adapter = getFetchAdapterFactory()
                .createAdapter(
                        sourceSettings.getAdapterName(),
                        sourceSettings.getAdapterContext()
                );

        if(sourceSettings.getAdapterContext() != null) {
            String oldWorkPath = workPath;
            workPath = oldWorkPath + sourceSettings.getAdapterContext().hashCode() + File.separator ;
            File workFolder = new File(workPath);
            if(!(workFolder.mkdirs() || workFolder.exists())){
                workPath = oldWorkPath; //Shouldn't we throw an exception? Or a warning maybe?
            }
        }
        List<String> localFiles = adapter.fetch(sourceSettings.getFileMasks(), workPath);

        return convertLocalFilesToEntity(
                localFiles != null
                        ? localFiles
                        : new ArrayList<>(),
                sourceSettings
        );
    }

    /**
     * Fetch all available logs from services which provides in settings.
     * @param sourceSettingsList List of settings which provides a settings for services.
     * @return List of log files.
     */
    @Override
    public List<InputLogFileEntity> fetch(List<SourceSettingsEntity> sourceSettingsList) throws FetchFailedException {
        if (sourceSettingsList == null) {
            throw new ArgumentNullException("sourceSettingsList");
        }

        List<InputLogFileEntity> fileEntities = new ArrayList<>();

        for(SourceSettingsEntity sourceSetting : sourceSettingsList) {
            try {
                List<InputLogFileEntity> fetchedEntities = fetch(sourceSetting, getBaseWorkingDirectoryPath());
                if(!fetchedEntities.isEmpty()) {
                    fileEntities.addAll(fetchedEntities);
                } else throw new FetchFailedException("No files were fetched","", sourceSetting.getAdapterName());
            } catch (FetchFailedException e) {
                if(isIgnoreErrors()) {
                    // TODO Refactor this (?)
                    if(e.getNode().isEmpty()) {
                        System.out.printf("An error occurred during fetching with %s adapter.\n", sourceSetting.getAdapterName());
                    } else {
                        System.out.printf("An error occurred during fetching from %s with %s adapter.\n",
                                e.getNode(), sourceSetting.getAdapterName());
                    }
                    System.out.printf("Description: \"%s\"\nSkipping this context.\n", e.getMessage());
                } else {
                    throw e;
                }
            }
        }

        if(fileEntities.isEmpty()) throw new FetchFailedException("No files were fetched","all nodes","any");

        // Remove duplicates from entities list
        Set<InputLogFileEntity> fileEntitiesSet = new HashSet<>(fileEntities);
        return new ArrayList<>(fileEntitiesSet);
    }
}
