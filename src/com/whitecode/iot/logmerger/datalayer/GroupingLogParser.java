/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogParser;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for parsing log strings of specified format.
 * @version 1.0
 */
public class GroupingLogParser implements ILogParser {
    public static final String STR_GROUP_DATETIME = "date";
    public static final String STR_GROUP_DEVICE = "device";
    public static final String STR_GROUP_TYPE = "type";
    public static final String STR_GROUP_MESSAGE = "message";

    /**
     * Pattern object that is used to compile the regular expression and create a Matcher.
     */
    private Pattern patternMatch;

    /**
     * Object that is used to parse date and time
     */
    private final DateTimeFormatter dateTimeFormatter;

    /**
     * Constructor that uses English locale by default.
     * @param logFormat - regular expression representing the log entry format.
     * @param dateTimeFormat - string representing the date and time format.
     */
    public GroupingLogParser(String logFormat, String dateTimeFormat) {
        this(logFormat, dateTimeFormat, Locale.ENGLISH);
    }

    /**
     * Constructor that uses a specified locale.
     * @param logFormat - regular expression representing the log entry format.
     * @param dateTimeFormat - string representing the date and time format.
     * @param locale - specified locale.
     */
    public GroupingLogParser(String logFormat, String dateTimeFormat, Locale locale) throws IllegalArgumentException  {
        if(logFormat == null) {
            throw new ArgumentNullException("logFormat");
        }
        if(dateTimeFormat == null) {
            throw new ArgumentNullException("dateTimeFormat");
        }
        if(locale == null) {
            throw new ArgumentNullException("locale");
        }
        this.patternMatch = Pattern.compile(logFormat);
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeFormat).withLocale(locale);
    }

    /**
     * Method that implements parsing of the given string.
     * @param strLine - given string.
     * @return a LogEntryEntity object that contains information from the given string.
     * @throws IOException when the given string cannot be parsed.
     */
	@Override
    public LogEntryEntity parse(String strLine) throws IOException, IllegalArgumentException {
        if(strLine == null || strLine.isEmpty()) {
            throw new ArgumentNullException("strLine");
        }

        try {
            Matcher matcher = patternMatch.matcher(strLine);
            if (matcher.matches()) {
                String strDateTime = matcher.group(STR_GROUP_DATETIME);
                String strDevice = matcher.group(STR_GROUP_DEVICE);
                String strType = matcher.group(STR_GROUP_TYPE);
                String strMessage = matcher.group(STR_GROUP_MESSAGE);

                LocalDateTime datetime = LocalDateTime.parse(strDateTime, dateTimeFormatter);
                return new LogEntryEntity(datetime, strDevice, strType, strMessage);
            } else {
                throw new IOException("Failed to parse the string \"" + strLine + "\": cannot parse this log format!");
            }
        } catch(DateTimeParseException e) {
            throw new IOException("Failed to parse the string \"" + strLine + "\": cannot parse date or time!");
        }
    }

    /**
     * Used pattern getter method.
     * @return - the pattern object used in the current parser.
     */
    public Pattern getPatternMatch() {
        return patternMatch;
    }
}
