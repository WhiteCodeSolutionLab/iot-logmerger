/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogReader;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.ThreadPoolManager;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Buffer manager for log reading in separate thread.
 *
 * @version 1.1
 */
public class BufferedLogReader implements ILogReader, Runnable {
    /**
     * Current future task.
     */
    private Future taskFuture = null;

    /**
     * Get current future task.
     *
     * @return Current future task.
     */
    protected Future getTaskFuture() {
        return taskFuture;
    }

    /**
     * Set current future task.
     */
    private void setTaskFuture(Future future) {
        taskFuture = future;
    }

    private List<LogEntryEntity> bufferList;

    /**
     * Get a current buffer.
     *
     * @return The list buffer.
     */
    public List<LogEntryEntity> getBufferList() {
        return bufferList;
    }

    /**
     * Max buffer size.
     */
    private int maxBufferSize;

    /**
     * Gets a current max buffer size.
     *
     * @return The max buffer size.
     */
    private int getMaxBufferSize() {
        return maxBufferSize;
    }

    /**
     * Real log reader stream for current entry entity.
     */
    private ILogReader logReader;

    /**
     * Flag indicates that stream is end.
     */
    private boolean isEnd;

    /**
     * Initialize a new instance of the log reader buffer manager.
     *
     * @param logReader    Real log reader.
     * @param maxBufferSize Max buffer size.
     */
    public BufferedLogReader(ILogReader logReader, int maxBufferSize) {
        if (logReader == null) {
            throw new ArgumentNullException("logReader");
        }

        if (maxBufferSize <= 0) {
            throw new IllegalArgumentException("Parameter 'maxBufferSize' should be greater that zero");
        }

        this.maxBufferSize = maxBufferSize;
        bufferList = Collections.synchronizedList(new LinkedList<>());
        this.logReader = logReader;
        isEnd = false;
    }

    public BufferedLogReader(ILogReader logReader) {
        this(logReader, 1000);
    }

    /**
     * Read log entry entity from input stream while buffer is not full
     */
    @Override
    public void run() {
        while (!isBufferFull() && !isEnd) {
            try {
                readToBuffer();
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
        setTaskFuture(null);
    }

    /**
     * Read log entry entity from real reader and add to buffer.
     *
     * @return True when read successfully and false when read null.
     * @throws IOException
     */
    private boolean readToBuffer() throws IOException {
        LogEntryEntity logEntryEntity = logReader.ReadEntry();
        if (logEntryEntity == null) {
            isEnd = true;
            return false;
        }
        getBufferList().add(logEntryEntity);
        return true;
    }

    /**
     * Check that buffer size less then max buffer size.
     *
     * @return True when buffer size less then max buffer size.
     */
    private boolean isBufferFull() {
        return (getBufferList().size() >= getMaxBufferSize());
    }

    /**
     * Read log entry entity from buffer.
     *
     * @return log entry entity.
     * @throws IOException
     */
    @Override
    public LogEntryEntity ReadEntry() throws IOException {
        while (getBufferList().size() <= 0 && !isEnd) {
            if (getTaskFuture() == null || getTaskFuture().isDone()) {
                setTaskFuture(ThreadPoolManager.submit(this));
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (getBufferList().size() <= 0 && isEnd) {
            return null;
        }
        if (getBufferList().size() > 0) {
            LogEntryEntity logEntryEntity = getBufferList().get(0);
            getBufferList().remove(0);
            return logEntryEntity;
        }
        return null;
    }

    /**
     * Shout down this reader.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        if(getTaskFuture()!= null){
            getTaskFuture().cancel(true);
        }
        logReader.close();
    }
}
