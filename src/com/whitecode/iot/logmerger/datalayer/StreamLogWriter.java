/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogFormatter;
import com.whitecode.iot.logmerger.datalayer.interfaces.ILogWriter;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.io.*;

/**
 * Implements a log writer to stream data storage.
 * @version 1.0
 */
public class StreamLogWriter implements ILogWriter{

    /**
     * The current output stream which used as data storage for log entries.
     */
    private OutputStreamWriter outputWriter;

    /**
     * Gets a current output stream which used as data storage for log entries.
     *
     * @return The output stream writer.
     */
    protected OutputStreamWriter getOutputStream() {
        return outputWriter;
    }

    private ILogFormatter logFormatter;

    /**
     * Initialize a new instance of the stream log writer
     * which used output stream as data storage for log entries.
     * Set a date time format for output.
     *
     * @param outputStream The output stream.
     */
    public StreamLogWriter(OutputStream outputStream, ILogFormatter logFormatter) {
        if (outputStream == null) {
            throw new ArgumentNullException("outputStream");
        }

        if (logFormatter == null) {
            throw new ArgumentNullException("logFormatter");
        }

        this.outputWriter = new OutputStreamWriter(outputStream);
        this.logFormatter = logFormatter;
    }

    /**
     * Write log entry to data storage.
     *
     * @param logEntryEntity The log entry entity. This parameter can not be NULL.
     * @throws IllegalArgumentException
     * @throws IOException
     */
    @Override
    public void WriteEntry(LogEntryEntity logEntryEntity) throws IllegalArgumentException, IOException {
        if (logEntryEntity == null) {
            throw new ArgumentNullException("logEntryEntity");
        }

        String line = logFormatter.format(logEntryEntity);

        getOutputStream().write(line + "\n");
        getOutputStream().flush();
    }

    /**
     * Close and free all system resources.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        getOutputStream().close();
    }
}
