/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer.fetch.adapters;

import com.whitecode.iot.logmerger.datalayer.interfaces.IFetchContext;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents a configuration context for SshFetchAdapter.
 * @version 1.1
 */
public class SshFetchContext implements IFetchContext {
    private static final String WRONG_CONTEXT_FORMAT_EXCEPTION_MESSAGE = "Wrong format for context JSON format";
    private static final String FIELD_NOT_EXISTS_FORMAT_EXCEPTION_MESSAGE = WRONG_CONTEXT_FORMAT_EXCEPTION_MESSAGE + ". Field [%s] not found in JSON object!";

    private static final String HOST_JSON_KEY = "host";
    private static final String PORT_JSON_KEY = "port";
    private static final String AUTH_TYPE_JSON_KEY = "authType";
    private static final String USER_JSON_KEY = "user";
    private static final String PASSPHRASE_JSON_KEY = "password";
    private static final String KEYFILE_JSON_KEY = "keyfile";

    private static final SshAuthType DEFAULT_AUTH_TYPE = SshAuthType.normal;

    private String host;
    private int port;
    private String user;
    private SshAuthType authType;
    private String passphrase = null;
    private String privateKeyFilePath = null;

    /**
     * Gets a host for connection to ssh console. It's can be presents as hostname (local.ssh.host) or ip (127.0.0.1).
     * @return The host.
     */
    public String getHost() {
        return host;
    }

    private void setHost(String host) {
        if (host == null) {
            throw new ArgumentNullException("host");
        }

        this.host = host;
    }

    /**
     * Gets a port for connection to ssh console.
     * @return The port number.
     */
    public int getPort() {
        return port;
    }

    private void setPort(int port) {
        if (port <= 0) {
            throw new IllegalArgumentException("port should be greater than 0");
        }

        this.port = port;
    }

    /**
     * Gets user name which used for connection to ssh console.
     * @return The user name.
     */
    public String getUser() {
        return user;
    }

    private void setUser(String user) {
        if (user == null) {
            throw new ArgumentNullException("user");
        }

        this.user = user;
    }

    /**
     * Gets a passphrase for connection to ssh console.
     * @return The passphrase.
     */
    public String getPassphrase() {
        return passphrase;
    }

    private void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    /**
     * Gets an authentication type for connection to ssh console.
     * @return The authType.
     */
    public SshAuthType getAuthType() {
        return authType;
    }

    private void setAuthType(SshAuthType authType) {
        if (authType == null) {
            throw new ArgumentNullException("authType");
        }

        this.authType = authType;
    }

    /**
     * Gets a private key file path for connection to ssh console.
     * @return The key file path.
     */
    public String getPrivateKeyFilePath() {
        return privateKeyFilePath;
    }

    private void setKeyFilePath(String keyFile) {
        this.privateKeyFilePath = keyFile;
    }

    /**
     * Create a new instance of the ssh fetch context using direct parameters.
     * @param host The host name.
     * @param port The port number.
     * @param user The user name.
     * @param type The authentication type.
     * @param passphrase The password or passphrase.
     * @param privateKeyFilePath The path to the private key file.
     */
    public SshFetchContext(String host, int port, String user, SshAuthType type, String passphrase, String privateKeyFilePath) {
        setHost(host);
        setPort(port);
        setUser(user);
        setAuthType(type);
        setPassphrase(passphrase);
        setKeyFilePath(privateKeyFilePath);
    }

    private static void validateFieldParameters(JSONObject jsonObject, String key) {
        if (jsonObject == null) {
            throw new ArgumentNullException("jsonObject");
        }

        if (key == null || key.length() == 0) {
            throw new ArgumentNullException("key");
        }

        if (!jsonObject.has(key)) {
            throw new IllegalArgumentException(String.format(FIELD_NOT_EXISTS_FORMAT_EXCEPTION_MESSAGE, key));
        }
    }

    private static String parseStringField(JSONObject jsonObject, String key) {
        validateFieldParameters(jsonObject, key);

        return jsonObject.getString(key);
    }

    private static int parseIntField(JSONObject jsonObject, String key) {
        validateFieldParameters(jsonObject, key);

        return jsonObject.getInt(key);
    }

    /**
     * Create a new instance of the ssh context from JSON string.
     * @param context The context settings represented as JSON string.
     */
    public static SshFetchContext fromJsonString(String context) {
        if (context == null) {
            throw new ArgumentNullException("context");
        }

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(context);
        } catch (JSONException ex) {
            throw new IllegalArgumentException("Wrong context JSON format", ex);
        }

        int port;
        String  host,
                username,
                passphrase = null,
                keyFilePath = null;

        SshAuthType authType = DEFAULT_AUTH_TYPE;

        host = parseStringField(jsonObject, HOST_JSON_KEY);
        port = parseIntField(jsonObject, PORT_JSON_KEY);

        if(jsonObject.has(AUTH_TYPE_JSON_KEY)) {
            String strType = parseStringField(jsonObject, AUTH_TYPE_JSON_KEY);
            try {
                authType = SshAuthType.valueOf(strType);
            } catch(IllegalArgumentException ex) {
                authType = DEFAULT_AUTH_TYPE;
            }
        }

        username = parseStringField(jsonObject, USER_JSON_KEY);

        switch (authType) {
            case privateKey:
                keyFilePath = parseStringField(jsonObject, KEYFILE_JSON_KEY);
            case normal:
                passphrase = parseStringField(jsonObject, PASSPHRASE_JSON_KEY);
                break;
        }

        return new SshFetchContext(host, port, username, authType, passphrase, keyFilePath);
    }
}
