/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer.fetch.adapters;

import com.jcraft.jsch.*;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.FetchFailedException;
import com.whitecode.iot.logmerger.datalayer.BaseFetchAdapter;

import java.io.File;
import java.util.Arrays;
import java.util.List;


/**
 * Implement a fetch adapter for SSH client.
 * @version 1.1
 */
public class SshFetchAdapter extends BaseFetchAdapter<SshFetchContext> {
    /**
     * Create a new instance of the file fetch service.
     * @param context The instance of the ssh fetch context
     */
    public SshFetchAdapter(SshFetchContext context) {
        super(context);
    }

    /**
     * Fetch files through SSH connection.
     * @param fileMasks list of the file masks required for fetching from the service.
     * @param workPath path to store the fetched files.
     * @return list of the local files' paths.
     */
    @Override
    public List<String> fetch(List<String> fileMasks, String workPath) throws FetchFailedException {
        validateContext();
        if (fileMasks == null) {
            throw new ArgumentNullException("fileMasks");
        }

        if (workPath == null) {
            throw new ArgumentNullException("workPath");
        }

        List<String> fetchedFilesPaths = null;
        try {
            JSch jsch = new JSch();
            SshFetchContext context = getContext();
            Session session = jsch.getSession(context.getUser(), context.getHost(), context.getPort());
            session.setConfig("StrictHostKeyChecking", "no");

            SshAuthType authType = context.getAuthType();
            switch (authType) {
                case privateKey: jsch.addIdentity(context.getPrivateKeyFilePath(), context.getPassphrase()); break;
                case normal: session.setPassword(context.getPassphrase()); break;
                default: break;
            }


            try {
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect(/*maybe set timeout*/);
                ChannelSftp sftpChannel = (ChannelSftp) channel;

                for(String src : fileMasks) {
                    src = src.replace("\\","/");
                    sftpChannel.get(src, workPath);
                }
                channel.disconnect();

                File workFolder = new File(workPath);
                String[] filesNames = workFolder.list();
                for(int i = 0; i < filesNames.length; i++) {
                    filesNames[i] = workPath + filesNames[i];
                }
                fetchedFilesPaths = Arrays.asList(filesNames);
            } finally {
                session.disconnect();
            }
        } catch (JSchException e) {
            throw new FetchFailedException("Can't establish SSH-connection: " + e.getMessage(),
                                            getContext().getHost(), "ssh", e);
        } catch (SftpException e) {
            throw new FetchFailedException("Can't get file through SFTP: SftpExcepction " + e.toString(),
                    getContext().getHost(), "ssh", e);
        }
        return fetchedFilesPaths;
    }

    private void validateContext() throws FetchFailedException {
        SshFetchContext context = getContext();
        if(context.getAuthType() == SshAuthType.privateKey) {
            if (context.getPrivateKeyFilePath() == null || context.getPrivateKeyFilePath().isEmpty()) {
                throw new FetchFailedException("Invalid context: no key file path for 'privateKey' authentication type",
                        context.getHost(), "ssh");
            }
        }
    }
}
