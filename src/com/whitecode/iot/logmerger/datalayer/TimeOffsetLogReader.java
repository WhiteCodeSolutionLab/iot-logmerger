/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogReader;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Log reader for applying a time offset.
 * @version 1.1
 */
public class TimeOffsetLogReader implements ILogReader {

    private LocalDateTime timeOffset;

    public LocalDateTime getTimeOffset() {
        return timeOffset;
    }

    private boolean signTimeOffset;

    public boolean getSignTimeOffset() {
        return signTimeOffset;
    }

    private ILogReader logReader;

    public ILogReader getLogReader() {
        return logReader;
    }

    public TimeOffsetLogReader(LocalDateTime timeOffset, boolean signTimeOffset, ILogReader logReader) {
        if (timeOffset == null) {
            throw new ArgumentNullException("TimeOffset");
        }
        if (logReader == null) {
            throw new ArgumentNullException("LogReader");
        }

        this.timeOffset = timeOffset;
        this.signTimeOffset = signTimeOffset;
        this.logReader = logReader;
    }


    @Override
    public LogEntryEntity ReadEntry() throws IOException {
        LogEntryEntity logEntry;
        logEntry = logReader.ReadEntry();
        if (logEntry != null) {
            if (signTimeOffset) {
                logEntry.setTimestamp(plusTimeOffset(logEntry.getTimestamp()));
            } else {
                logEntry.setTimestamp(minusTimeOffset(logEntry.getTimestamp()));
            }
        }
        return logEntry;
    }

    private LocalDateTime plusTimeOffset(LocalDateTime entryTime) {
        return entryTime.plusNanos(timeOffset.getNano())
                .plusSeconds(timeOffset.getSecond())
                .plusMinutes(timeOffset.getMinute())
                .plusHours(timeOffset.getHour())
                .plusDays(timeOffset.getDayOfYear())
                .plusYears(timeOffset.getYear());

    }

    private LocalDateTime minusTimeOffset(LocalDateTime entryTime) {
        return entryTime.minusNanos(timeOffset.getNano())
                .minusSeconds(timeOffset.getSecond())
                .minusMinutes(timeOffset.getMinute())
                .minusHours(timeOffset.getHour())
                .minusDays(timeOffset.getDayOfYear())
                .minusYears(timeOffset.getYear());
    }

    @Override
    public void close() throws IOException {
        logReader.close();
    }
}
