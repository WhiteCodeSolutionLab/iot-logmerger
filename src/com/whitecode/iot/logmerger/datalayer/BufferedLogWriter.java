/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogWriter;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;
import com.whitecode.iot.logmerger.infrastructure.ThreadPoolManager;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Buffer manager for log writing in separate thread.
 *
 * @version 1.1
 */
public class BufferedLogWriter implements ILogWriter, Runnable {
    /**
     * Current future task.
     */
    private Future taskFuture = null;

    /**
     * Get current future task.
     *
     * @return Current future task.
     */
    protected Future getTaskFuture() {
        return taskFuture;
    }

    /**
     * Set current future task.
     */
    private void setTaskFuture(Future future) {
        taskFuture = future;
    }

    /**
     * Current buffer list.
     */
    private List<LogEntryEntity> bufferList;

    /**
     * Get a current buffer.
     *
     * @return The list buffer.
     */
    public List<LogEntryEntity> getBufferList() {
        return bufferList;
    }

    /**
     * Max buffer size.
     */
    private int maxBufferSize;

    /**
     * Gets a current max buffer size.
     *
     * @return The max buffer size.
     */
    public int getMaxBufferSize() {
        return maxBufferSize;
    }

    /**
     * Real log writer stream  for current entry entity.
     */
    private ILogWriter logWriter;

    /**
     * Gets a current log writer stream.
     *
     * @return Current log writer.
     */
    public ILogWriter getLogWriter() {
        return logWriter;
    }

    /**
     * Initialize a new instance of the stream log writer buffer manager in new tread.
     *
     * @param logWriter    Real log writer.
     * @param maxBufferSize Max buffer size.
     */
    public BufferedLogWriter(ILogWriter logWriter, int maxBufferSize) {
        if (logWriter == null) {
            throw new ArgumentNullException("logWriter");
        }

        if (maxBufferSize <= 0) {
            throw new IllegalArgumentException("Parameter 'maxBufferSize' should be greater that zero");
        }

        this.maxBufferSize = maxBufferSize;
        bufferList = Collections.synchronizedList(new LinkedList<>());
        this.logWriter = logWriter;
    }

    /**
     * Initialize a new instance of the log writer buffer manager in new tread.
     *
     * @param logWriter Real log whiter.
     */
    public BufferedLogWriter(ILogWriter logWriter) {
        this(logWriter, 1000);
    }


    /**
     * Referent log entry entity to log stream writer when buffer is not empty.
     */
    @Override
    public void run() {
        while (!getBufferList().isEmpty()) {
            try {
                getLogWriter().WriteEntry(getBufferList().get(0));
                getBufferList().remove(0);
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    /**
     * Check a state of buffer, when buffer is not full,
     * add log entry entity to buffer.
     *
     * @param logEntryEntity The log entry entity. This parameter can not be NULL.
     * @throws IllegalArgumentException
     * @throws IOException
     */
    @Override
    public void WriteEntry(LogEntryEntity logEntryEntity) throws IllegalArgumentException, IOException {
        if (logEntryEntity == null) {
            throw new ArgumentNullException("logEntryEntity");
        }

        while (isBufferFull()) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        getBufferList().add(logEntryEntity);

        if (getTaskFuture() == null || getTaskFuture().isDone()) {
            setTaskFuture(ThreadPoolManager.submit(this));
        }
    }

    /**
     * Check that buffer size less then max buffer size.
     *
     * @return True when buffer size less then max buffer size.
     */
    private boolean isBufferFull() {
        return (getBufferList().size() >= getMaxBufferSize());
    }

    /**
     * Shout down this writer.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        if (getTaskFuture() != null) {
            while (!getTaskFuture().isDone()) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        getLogWriter().close();
    }
}
