/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogParser;
import com.whitecode.iot.logmerger.datalayer.interfaces.ILogReader;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.io.*;

/**
 * Implements a log reader from stream data source.
 * @version 1.0
 */
public class StreamLogReader implements ILogReader {
    /**
     * The current input stream which used as data source for this reader.
     */
    private InputStreamReader inputStreamReader;

    /**
     * The current input stream which used as data source for this reader.
     * @return The input stream.
     */
    private InputStreamReader getInputStreamReader() {
        return inputStreamReader;
    }

    /**
     * The buffered reader.
     */
    private BufferedReader bufferedReader;

    /**
     * Gets the buffered reader.
     * @return The buffered reader.
     */
    private BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    /**
     * Current parser for parsing strings of of specified format.
     */
    private ILogParser parser;

    /**
     * Get a current parser.
     * @return Current log parser.
     */
    private ILogParser getParser() {
        return parser;
    }

    /**
     * Initialize a new instance with specific input stream which will be used as data source for reading a new log entry entities.
     * @param inputStream The input stream.
     */
    public StreamLogReader(InputStream inputStream, ILogParser parser) {
        if (inputStream == null) {
            throw new ArgumentNullException("inputStreamReader");
        }

        if (parser == null) {
            throw new ArgumentNullException("parser");
        }

        this.inputStreamReader = new InputStreamReader(inputStream);
        this.bufferedReader = new BufferedReader(inputStreamReader);
        this.parser = parser;
    }

    /**
     * Read string line from stream and try to parse it to log entry entity.
     * @return Log entry entity or null when line in stream is null.
     * @throws IOException
     */
    @Override
    public LogEntryEntity ReadEntry() throws IOException {
        String strLine = getBufferedReader().readLine();
        if(strLine == null) {
            return null;
        }

        return getParser().parse(strLine);
    }

    /**
     * Close and free all system resource.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        getInputStreamReader().close();
        getBufferedReader().close();
    }
}
