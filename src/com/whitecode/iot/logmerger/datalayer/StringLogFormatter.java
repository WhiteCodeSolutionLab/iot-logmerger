/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.datalayer;

import com.whitecode.iot.logmerger.datalayer.interfaces.ILogFormatter;
import com.whitecode.iot.logmerger.entities.LogEntryEntity;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.time.format.DateTimeFormatter;


/**
 * Implement methods to carry out log data to output format.
 * @version 1.0
 */
public class StringLogFormatter implements ILogFormatter {

    public static final String STR_PARAMETER_DATE         =   "%date%";
    public static final String STR_PARAMETER_DEVICE       =   "%device%";
    public static final String STR_PARAMETER_MESSAGE_TYPE =   "%type%";
    public static final String STR_PARAMETER_MESSAGE      =   "%message%";

    private String template;

    /**
     * The current output date time format.
     */
    private DateTimeFormatter dateTimeFormatter;

    /**
     * Gets a current output date time format.
     */
    public DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }

    /**
     * Create new instance of string log formatter.
     * @param dateTimeFormatter Date time formatter.
     * @param template String template of format for log entry entity.
     */
    public StringLogFormatter(DateTimeFormatter dateTimeFormatter, String template){
        if(dateTimeFormatter == null){
            throw new ArgumentNullException("dateTimeFormatter");
        }
        if((template == null)||(template.isEmpty())){
            throw new ArgumentNullException("template");
        }

        this.dateTimeFormatter = dateTimeFormatter;
        this.template = template;
    }

    /**
     * Create new instance of string log formatter.
     * @param dateTimeFormatter Date time formatter.
     */
    public StringLogFormatter(DateTimeFormatter dateTimeFormatter){
        this(dateTimeFormatter, "%date% [%device%] %type% - %message%");
    }

    /**
     * Format log entry entity to special format.
     * @param logEntryEntity Log entry entity for format.
     * @return Log entry entity in special format in string.
     */
    @Override
    public String format(LogEntryEntity logEntryEntity) {
        if(logEntryEntity == null){
            throw new ArgumentNullException("logEntryEntity");
        }
        String line = template;
        line = line.replace(STR_PARAMETER_DATE, logEntryEntity.getTimestamp().format(getDateTimeFormatter()));
        line = line.replace(STR_PARAMETER_DEVICE, logEntryEntity.getDeviceName());
        line = line.replace(STR_PARAMETER_MESSAGE_TYPE, logEntryEntity.getMessageType());
        line = line.replace(STR_PARAMETER_MESSAGE, logEntryEntity.getMessage());

        return line;
    }

    /**
     * Format log entry entity to special format.
     * @param logEntryEntity Log entry entity for format.
     * @return Log entry entity in special format in array of byte.
     */
    @Override
    public byte[] formatToByteArray(LogEntryEntity logEntryEntity){
        if(logEntryEntity == null){
            throw new ArgumentNullException("logEntryEntity");
        }

        return this.format(logEntryEntity).getBytes();
    }
}
