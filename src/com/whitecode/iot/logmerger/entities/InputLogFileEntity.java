/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.entities;

import java.time.LocalDateTime;

/**
 * Represents a configuration entity which contains information about input log files.
 * @version 1.0
 */
public class InputLogFileEntity {
    private String filePath;
    private String logFormatType;
    private LocalDateTime timeOffset;
    private boolean timeOffsetSign;

    /**
     * Gets a source log file path.
     * @return The file path.
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Gets a source log format type which used for this file.
     * @return The log format type.
     */
    public String getLogFormatType() {
        return logFormatType;
    }

    public LocalDateTime getTimeOffset(){
        return timeOffset;
    }

    public boolean getTimeOffsetSign(){
        return timeOffsetSign;
    }

    /**
     * Create a new instance of the source log files.
     * @param filePath The file path.
     * @param logFormatType The log format type.
     */
    public InputLogFileEntity(String filePath, String logFormatType, LocalDateTime timeOffset, boolean timeOffsetSign) {
        this.filePath = filePath;
        this.logFormatType = logFormatType;
        this.timeOffset = timeOffset;
        this.timeOffsetSign = timeOffsetSign;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;
        return this.filePath.equals(((InputLogFileEntity) obj).filePath)
                && this.logFormatType.equals(((InputLogFileEntity) obj).logFormatType);
    }

    @Override
    public int hashCode() {
        return filePath.hashCode() + logFormatType.hashCode();
    }
}
