/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.entities;

import java.time.LocalDateTime;
import com.whitecode.iot.logmerger.exceptions.ArgumentNullException;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a source settings which provides logs for merging.
 *
 * @version 1.1
 */
public class SourceSettingsEntity {
    private List<String> fileMasks;
    private String logFormatType;
    private String adapterName;
    private String adapterContext;

    private boolean timeOffsetSign;


    private LocalDateTime timeOffset;

    /**
     * Gets a list of file masks.
     *
     * @return The list of file masks.
     */
    public List<String> getFileMasks() {
        return fileMasks;
    }

    /**
     * Gets a log format type which used for source logs.
     *
     * @return The log format type.
     */
    public String getLogFormatType() {
        return logFormatType;
    }

    /**
     * Gets a adapter name which using for fetch source logs from specific service.
     *
     * @return The adapter name.
     */
    public String getAdapterName() {
        return adapterName;
    }

    /**
     * Gets a adapter context string which contains settings for specific service.
     *
     * @return The adapter context.
     */
    public String getAdapterContext() {
        return adapterContext;
    }
    public LocalDateTime getTimeOffset() {
        return timeOffset;
    }

    public boolean getTimeOffsetSign() {
        return timeOffsetSign;
    }
    /**
     * Create a new instance of source settings entity.
     *
     * @param fileMasks The list of file masks.
     * @param logFormatType The log format type.
     * @param adapterName The adapter name.
     * @param adapterContext The adapter context.
     */
    public SourceSettingsEntity(List<String> fileMasks, String logFormatType, String adapterName, String adapterContext, LocalDateTime timeOffset, boolean timeOffsetSign) {
        if (fileMasks == null) {
            throw new ArgumentNullException("fileMasks");
        }

        this.fileMasks = new ArrayList<>(fileMasks);
        this.logFormatType = logFormatType;
        this.adapterName = adapterName;
        this.adapterContext = adapterContext;

        this.timeOffset = timeOffset;
        this.timeOffsetSign = timeOffsetSign;
    }

    /**
     * Create a new instance of source settings entity.
     *
     * @param fileMasks      The list of file masks.
     * @param logFormatType  The log format type.
     * @param adapterName    The adapter name.
     * @param adapterContext The adapter context.
     */
    public SourceSettingsEntity(List<String> fileMasks, String logFormatType, String adapterName, String adapterContext) {
        this(fileMasks, logFormatType, adapterName, adapterContext, LocalDateTime.of(0, 0, 0, 0, 0), false);
    }


}
