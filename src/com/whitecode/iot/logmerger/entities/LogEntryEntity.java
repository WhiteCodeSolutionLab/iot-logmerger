/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger.entities;

import java.time.LocalDateTime;

/**
 * Represents a log entry data structure.
 * @version 1.0
 */
public class LogEntryEntity implements Comparable<LogEntryEntity> {
    private LocalDateTime timestamp;
    private String deviceName;
    private String messageType;
    private String message;

    /**
     * Create a new instance of the log entry entity.
     *
     * @param timestamp The time when this entry will created.
     * @param deviceName The device name which triggered this log entry.
     * @param messageType The message type.
     * @param message The log entry message.
     */
    public LogEntryEntity(LocalDateTime timestamp, String deviceName, String messageType, String message) {
        this.timestamp = timestamp;
        this.deviceName = deviceName;
        this.messageType = messageType;
        this.message = message;
    }

    /**
     * Gets the time when this entry will created.
     *
     * @return The date time.
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets the device name which triggered this log entry.
     *
     * @return The device name.
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Gets the message type of this entry.
     *
     * @return The message type.
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Gets the log entry message.
     *
     * @return Them message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Overridden method of comparison between log entry entities.
     * @param anotherEntity - entity to compare.
     * @return result of LocalDateTime.compareTo() method.
     */
    @Override
    public int compareTo(LogEntryEntity anotherEntity) {
        return this.getTimestamp().compareTo(anotherEntity.getTimestamp());
    }
}
