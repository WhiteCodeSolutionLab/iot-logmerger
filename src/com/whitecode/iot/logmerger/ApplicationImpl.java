/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger;

import com.whitecode.iot.logmerger.datalayer.interfaces.*;
import com.whitecode.iot.logmerger.entities.*;
import com.whitecode.iot.logmerger.exceptions.*;
import com.whitecode.iot.logmerger.infrastructure.*;
import com.whitecode.iot.logmerger.infrastructure.filters.*;
import com.whitecode.iot.logmerger.infrastructure.interfaces.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version 1.2
 */
public final class ApplicationImpl {

    private static void validateOutputFile(File file, boolean forceModeEnabled) throws UserException {
        if (file == null) {
            throw new ArgumentNullException("file");
        }

        if (!forceModeEnabled) {
            if (file.exists()) {
                throw new UserException(String.format("File \"%s\" already exists", file.getPath()));
            }
        }

        if (file.exists() && !file.canWrite()) {
            throw new UserException(String.format("File \"%s\" is not writable", file.getPath()));
        }
    }

    private static void validateInputFile(File file) throws UserException {
        if (file == null) {
            throw new ArgumentNullException("file");
        }

        if (!file.exists()) {
            throw new UserException(String.format("File \"%s\" can't be found", file.getPath()));
        }

        if (!file.canRead()) {
            throw new UserException(String.format("File \"%s\" can't be read because application don't have permission to perform this action.", file.getPath()));
        }
    }

    public static FileSettingsProvider readConfig(String configFilePath) throws UserException {
        File configFile = new File(configFilePath);
        validateInputFile(configFile);

        String exceptionMessage = "Can't read config file";
        try {
            return new FileSettingsProvider(configFilePath);
        } catch (IllegalArgumentException exception) {
            exceptionMessage = exception.getMessage();
            Throwable cause = exception.getCause();
            if (cause != null) {
                exceptionMessage = exceptionMessage.concat("\t" + cause.getMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new UserException(exceptionMessage);
    }

    public static void validateConfigSettings(FileSettingsProvider settingProvider) throws UserException {
        if(settingProvider == null){
            throw new IllegalArgumentException();
        }
        File targetFile = new File(settingProvider.getTargetLogFilePath());
        validateOutputFile(targetFile, true);

        // TODO Validate input source logs
    }

    public static List<ILogReader> getLogReaders(List<InputLogFileEntity> sourceLogs, ILogDataProviderFactory factory) throws FileNotFoundException {
        List<ILogReader> logReaders = new ArrayList<>(sourceLogs.size());
        for (InputLogFileEntity sourceLog : sourceLogs) {
            ILogReader logReader = factory.getLogReader(sourceLog);
            logReaders.add(logReader);
        }
        return logReaders;
    }

    public static ILogDataProviderFactory getLogDataProviderFactory(ISettingsProvider settingsProvider) {
        LogDataProviderFactory factory = new LogDataProviderFactory(settingsProvider);

        ApplicationConfig.RegisterLogFormats(factory);

        return factory;
    }

    public static long logMerging(List<InputLogFileEntity> sourceLogs, ISettingsProvider settingsProvider) throws IOException{
        // Initialize components
        ILogDataProviderFactory factory = getLogDataProviderFactory(settingsProvider);
        List<ILogReader> logReaders = getLogReaders(sourceLogs, factory);
        ILogWriter logWriter = factory.getLogWriter(settingsProvider.getTargetLogFilePath());

        FilterFactory filterFactory = new FilterFactory();
        BaseFilter filter = filterFactory.buildFilter(settingsProvider.getFilter());

        ILogProcessorComponent logProcessorComponent = new LogProcessorComponent();
        logProcessorComponent.setFilter(filter);

        // Start merging
        long startTime = System.currentTimeMillis();
        logProcessorComponent.Merge(logReaders, logWriter);
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static IFetchAdapterFactory getFetchAdapterFactory() {
        FetchAdapterFactory factory = new FetchAdapterFactory();
        ApplicationConfig.RegisterFetchAdapters(factory);
        return factory;
    }

    public static IFetchProcessorComponent getFetchProcessor(IFetcherSettingsProvider fetcherSettingsProvider) {
        return new FetchProcessorComponent(getFetchAdapterFactory(), fetcherSettingsProvider);
    }

    public static List<InputLogFileEntity> fetchLogsFromSources(FileSettingsProvider settingProvider) throws UserException, FetchFailedException {
        List<SourceSettingsEntity> sourceSettingsEntities = settingProvider.getSourceSettingsEntities();
        if (sourceSettingsEntities == null) {
            throw new ArgumentNullException("sourceSettingsEntities");
        }

        if (sourceSettingsEntities.size() == 0) {
            return new ArrayList<>();
        }

        IFetchProcessorComponent fetchProcessorComponent = getFetchProcessor(settingProvider);

        try {
            return fetchProcessorComponent.fetch(sourceSettingsEntities);
        } catch (IllegalArgumentException exception) {
            throw new UserException(exception.getMessage());
        }
    }

}
