/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger;

import com.whitecode.iot.logmerger.datalayer.GroupingLogParser;
import com.whitecode.iot.logmerger.datalayer.StreamLogReader;
import com.whitecode.iot.logmerger.datalayer.fetch.adapters.LocalFetchAdapter;
import com.whitecode.iot.logmerger.datalayer.fetch.adapters.SshFetchAdapter;
import com.whitecode.iot.logmerger.datalayer.fetch.adapters.SshFetchContext;
import com.whitecode.iot.logmerger.entities.InputLogFileEntity;
import com.whitecode.iot.logmerger.infrastructure.FetchAdapterFactory;
import com.whitecode.iot.logmerger.infrastructure.LogDataProviderFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Class for config required class instances.
 * @version 1.2
 */
public final class ApplicationConfig {
    /**
     * Register all supported log formats to log data provider factory.
     * @param factory The instance of the log data provider factory.
     */
    public static void RegisterLogFormats(LogDataProviderFactory factory) {
        // Register "first" log format
        //      2015-10-04 02:11:58,699 [DataSender] TRACE - Start transmission
        factory.registerReader("first", (InputLogFileEntity inputLogFileEntity) -> {
            try {
                InputStream inputStream = new FileInputStream(inputLogFileEntity.getFilePath());
                return new StreamLogReader(inputStream, new GroupingLogParser(
                        "(?<" + GroupingLogParser.STR_GROUP_DATETIME + ">.*)\\s+\\[(?<"
                                + GroupingLogParser.STR_GROUP_DEVICE + ">.*)\\]\\s+(?<"
                                + GroupingLogParser.STR_GROUP_TYPE + ">.*)\\s+\\-\\s+(?<"
                                + GroupingLogParser.STR_GROUP_MESSAGE + ">.*)",
                        "yyyy-MM-dd HH:mm:ss,SSS"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        });

        // Register "second" log format
        //      <Oct 04, 2015 2:11:58,757 AM UTC> <Receiver> <trace> - Incoming transmission accepted
        factory.registerReader("second", (InputLogFileEntity inputLogFileEntity) -> {
            try {
                InputStream inputStream = new FileInputStream(inputLogFileEntity.getFilePath());
                return new StreamLogReader(inputStream, new GroupingLogParser(
                        "<(?<" + GroupingLogParser.STR_GROUP_DATETIME + ">[^>]*)>\\s+<(?<"
                                + GroupingLogParser.STR_GROUP_DEVICE + ">[^>]*)>\\s+<(?<"
                                + GroupingLogParser.STR_GROUP_TYPE + ">[^>]*)>\\s+\\-\\s+(?<"
                                + GroupingLogParser.STR_GROUP_MESSAGE + ">.*)",
                        "MMM dd, yyyy h:mm:ss,SSS a zzz"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        });
    }

    /**
     * Register all supported fetch adapters to specific instance of fetch adapters factory.
     * @param factory The instance of the fetch adapters factory.
     */
    public static void RegisterFetchAdapters(FetchAdapterFactory factory) {
        factory.setDefaultAdapterFunction(context -> new LocalFetchAdapter());

        factory.registerAdapter("ssh", context -> new SshFetchAdapter(SshFetchContext.fromJsonString(context)));
    }
}
