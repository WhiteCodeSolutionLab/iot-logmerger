/**
 * @license
 * Copyright (c) 2015,
 *      Alexey Mulyukin (alexprey@yandex.ru),
 *      Olga Kalyonova (olgakalyonova.ifmo@gmail.com),
 *      Georgiy Zhemelev (wws.dev@gmail.com).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.whitecode.iot.logmerger;

import com.whitecode.iot.logmerger.entities.InputLogFileEntity;
import com.whitecode.iot.logmerger.entities.SourceSettingsEntity;
import com.whitecode.iot.logmerger.infrastructure.FileSettingsProvider;

import java.util.List;

public final class Views {
    public static void printHelp() {
        System.out.println("Tool usage:");
        System.out.println("\tjava -jar iot-logmerger \"config.cfg\"");
        System.out.println();
        System.out.println("Configuration file example:");
        System.out.println(
                "{\n\ttargetLogFilePath: \"TARGET_FILE\"," +
                        "\n\tsourceLogFiles: [\n\t\t{\n\t\t\tfilePath: \"SOURCE_FILE\",\n\t\t\tformat: \"FILE_FORMAT\"\n\t\t}\n\t]\n}"
        );

        System.out.println();
        System.out.println("Configuration file parameters:");
        System.out.println("\tTARGET_FILE - result file with all logs");
        System.out.println("\tSOURCE_FILE - source file with logs");
        System.out.println("\tFILE_FORMAT - indicates which format is used for source log file. It should be a next values:");
        System.out.println("\t\t\"first\" - log format such as: \"2015-10-10 23:00:01,557 [DataSender] TRACE - data sent\"");
        System.out.println("\t\t\"second\" - log format such as: \"<Oct 10, 2015 10:54:43,615 PM UTC> <Reciver> <error> - Incoming transmission accepted\"");
    }

    public static void printSelectSourceSettings(List<SourceSettingsEntity> sourceSettings) {
        System.out.println(String.format("Source settings: %d", sourceSettings.size()));

        for (int i = 0; i < sourceSettings.size(); i++) {
            SourceSettingsEntity sourceSetting = sourceSettings.get(i);

            System.out.printf("\tFormat\t[%d]: %s\n", i, sourceSetting.getLogFormatType());
            System.out.printf("\tAdapter\t[%d]: %s\n", i,
                    sourceSetting.getAdapterName() != null
                            ? sourceSetting.getAdapterName()
                            : "<default>"
            );

            List<String> fileMasks = sourceSetting.getFileMasks();
            if (fileMasks.size() == 1) {
                System.out.printf("\tFile\t[%d]: \"%s\"\n", i, fileMasks.get(0));
            } else {
                System.out.printf("\tFiles\t[%d]: total %d {\n", i, fileMasks.size());
                for(String fileMask : fileMasks) {
                    System.out.printf("\t\t\"%s\"\n", fileMask);
                }
                System.out.println("\t}");
            }
            System.out.println();
        }
    }

    public static void printSelectedConfig(FileSettingsProvider settingProvider) {
        System.out.printf("Thread pool size: %d\n", settingProvider.getThreadPoolSize());
        System.out.println("Buffer size:");
        System.out.printf("\tReader:\t%d\n", settingProvider.getReaderMaxBufferSize());
        System.out.printf("\tWriter:\t%d\n", settingProvider.getWriterMaxBufferSize());
        System.out.printf("Is fetch ignore errors: %s\n", settingProvider.isFetchIgnoreErrors());
        System.out.printf("Working directory path: \"%s\"\n", settingProvider.getWorkDirectoryPath());
        System.out.printf("Target log file: \"%s\"\n", settingProvider.getTargetLogFilePath());

        List<SourceSettingsEntity> sourceSettings = settingProvider.getSourceSettingsEntities();
        printSelectSourceSettings(sourceSettings);
    }

    public static String convertMsToString(long mstime) {
        int sec = (int) ((mstime / 1000) % 60);
        int min = (int) ((mstime / (1000 * 60)) % 60);
        int hours = (int) (mstime / (1000 * 60 * 60));

        return String.format("%d h. %d m. %d s.", hours, min, sec);
    }

    public static void printResult(long time, FileSettingsProvider settingProvider) {
        System.out.println("Merge process successfully completed");
        System.out.println(String.format("Time taken for merge: %s (%d ms)", convertMsToString(time), time));
        System.out.println(String.format("Result file places at: %s", settingProvider.getTargetLogFilePath()));
    }

    public static void printSourceLogs(List<InputLogFileEntity> sourceLogs) {
        System.out.printf("Result files: total %d\n", sourceLogs.size());
        for(int i = 0; i < sourceLogs.size(); i++) {
            InputLogFileEntity sourceLogEntity = sourceLogs.get(i);
            System.out.printf("\tFile[%d]:\t%s\n", i, sourceLogEntity.getFilePath());
        }
    }
}
