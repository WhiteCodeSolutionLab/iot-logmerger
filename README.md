# README #

This utillity developed for fetching, filtering and merging all logs from your distributed applications.

# Versions #

## Version 1.1 ##

* Log entry filtering
* Fetching logs from many sources
* Supports a diffirent time offsets between log sources
* Performance optimization for log readers and writers

## Version 1.0 ##

* Log merging from many files with diffirent formats

# Key features #

## Abstract log reader and writer ##

This tools have a abstract architecture for supporting a many others format for logs. At now tools support only two readers and one format writer. But code of this tools is customazible.
We can implement interface `ILogReader` for supports other log formats for reading, for example: reader from DB, network stream or other.
Also we can implement interface `ILogWriter` for supports other log formats for writing, for example: writer to DataBase, network stream or send logs to specific API.

## Log fetching for multiple sources ##

This tool can fetch logs from many sources. Source can be is remote host. Tool use a abstract architecture for fetching. We can implement a `IFetchAdapter` for custom implementation and use it for fetching. For example we can create a client for downloading your logs from remote web server.

At now this tools supports only one fetch adapter.
Also tool have a default fetch adapter for load your logs from local file system.

### SFTP Fetch adapter ###

This adapter load your logs from SFTP protocol. This protocol based on SSH connection.
Adapter configurate by simple context settings and can authorize to server using

* only username
* username and passphrase
* username, passphrase and file with private key

### Log filtering ###

On the last stage, before log merging, tool can filterate logs. Filter is an interface, but tool have some class hierarchy. This hierarchy give a flexible settings for customize final filter.

Filters devides on two diffirent types:

* SimpleBinaryFilter - this filters compare specific field in log entry with some value or many value if possible
    * greater
    * greaterEqual
    * less
    * lessEqual
    * equal
    * notEqual
* ComplexFilter - this filters apply logic operation to few other filters
    * AND complex filter
    * OR complex filter

## Performance optimization ##

This tool use a multi threading architecture via `ThreadPool`. All log readers and writers use it for improve performance for reading from many logs at one time quant. We can configurate size of threads count in tool thread pool.
Also for improve performance we can configurate a buffer size for every log readers and writers.

# Contribution guidelines #

## Branch strategy ##

* Branch `master` uses for stable and release code.
* Branch `develope` uses for stable code.
* For implementing a feature create a new branch and implement it in our branch. After implementation complete, create a new pull request to merge our code to `develope` branch.

## Code review and code guidelines ##

* Each files should contain a LICENSE file on header of code file.
* All public classes, constructors, methods, fields should have comments with `@version` tag.
* All public methods should have a parameters checks.

# Who do I talk to? #

This project start implemented be WhiteCode Team in 2015.

* Alexey Mulyukin (alexprey@yandex.ru) - Team Lead
* Olga Kalyonova (olgakalyonova.ifmo@gmail.com) - Developer
* Georgiy Zhemelev (wws.dev@gmail.com) - Developer


# Configuration in config.cfg #

Конфигурационный файл оформляется следующим образом:

1. Вначале указывается имя целевого(выходного) файла в поле targetLogFilePath. Например: `targetLogFilePath: "result.log"`
2. Указывается рабочая директория:
    `workDirectoryPath: "work"`
3. Выставляется флаг игнорирования недоступных файлов:
    `fetchIgnoreErrors: true`
4. Далее в поле sourceLogFiles указываются файлы исходных данных. Они могут располагаться как локально, так и удалённо.
    1. Необходимо указать файл или группу файлов: `filePath: "log1.txt"` или `filePath: [ "log2.txt", "*.log" ]`
    2. Для указанного файла или группы указывается формат входных данных:
        * Формат "first" представляет собой строку вида:
            `2015-10-04 02:11:58,699 [DataSender] TRACE - Start transmission`
        * Формат "second" представляет собой строку вида: `<Oct 04, 2015 2:11:58,757 AM UTC> <Receiver> <trace> - Incoming transmission accepted`
    3. Указывается известное смещение времени. При запаздывании времени в исходном файле относительно сервера необходимо поставить вначале "+", а при опережении - "-": `timeOffset: "+0000-07-08T00:00:00.545"`
    4.Если файл располагается не локально, то так же необходимо указать следующие поля:
        1. Протокол доступа к удалённым файлам. На данный момент поддерживается: ssh. `adapter: "ssh"`
        2. Контекст доступа к удалённым файлам для адаптера ssh.
            1. IP-адрес удалённого хоста: `host: "192.168.1.113"`
            2. Порт на удалённом хосте: `port: 22`
            3. Способ авторизации. На данный момент поддерживаются: none, normal, privateKey. Важно соблюдать регистр букв. `authType: "privateKey"`
            4. Имя пользователя: `user: "wsuser"`
            5. Для типов авторизации normal и privateKey так же необходимо указать пароль: `password: "iot"`
            6. Для типа авторизации privateKey необходимо указать путь к приватному ключу: `keyfile: "mypk.ppk"`
5. Необязательный параметр - размер буфера чтения. Значение по умолчанию = 1000. `readerMaxBufferSize: 100`
6. Необязательный параметр - размер буфера записи. Значение по умолчанию = 1000. `writerMaxBufferSize: 5000`
7. Необязательный параметр - размер пула потоков. Значение по умолчанию = 15. `threadPoolSize: 120`

Пример готового конфигурационного файла:

    {
          targetLogFilePath: "result.log",
          workDirectoryPath: "work",
          fetchIgnoreErrors: "true",
          sourceLogFiles: [
               {
                    filePath: "log1.txt",
                    format: "first",
                    timeOffset: "+0000-07-08T00:00:00.545"
               },
               {
                   filePath: [
                        "big\\log2.txt",
                        "log3.txt"
                   ],
                   format: "second",
                   adapter: "ssh",
                   adapterContext: {
                        host: "192.168.1.113",
                        port: 22,
                        authType: "privateKey",
                        user: "wsuser",
                        password: "iot",
                        keyfile: "mypk.ppk"
                   }
    
               }
          ],
          readerMaxBufferSize: 100,
          writerMaxBufferSize: 5000,
          threadPoolSize: 120
     }